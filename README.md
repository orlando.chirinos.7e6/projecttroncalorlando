WORDLE-Idea

_**IMPORTANTE NO EDITAR EL ARCHIVO "matchesdata.txt".**_

WORDLE es un juego donde se tendrá que  adivinar una palabra de X cantidad de caracteres, los caracteres aparecerán en pantalla marcados con ciertas características las cuales indicarán qué tan cerca (o lejos) se estará de adivinar la palabra.

Para jugar WORDLE-Idea es necesario tener instalado "IntelliJ Idea", descargar el archivo de éste repositorio git y abrirlo en el software anteriormente indicado.

Para jugar WORDLE-Idea se debe introducir una palabra de 5 caracteres, en pantalla se indicará con un color determinado si el caracter es correcto o no, a continuación, una guía:

- Rojo: Caracter no encontrado en la palabra objetivo.

- Gris : Caracter encontrado en la palabra objetivo, pero no en la posición correcta.

- Verde: Caracter encontrado en la palabra objetivo y en la misma posición.


**FUNCIONES:**

main:  Menú principal, ejecuta la función decidenewgame.

**decidenewgame:** 

Deja al usuario varias opciones a elegir, al introducir una de éstas se ejecuta la función correspondiente, caso contrario se ejecuta nuevamente ésta misma función. Se pudo haber hecho con un simple WHEN, a corregir.
- L: Logros
- N: Nuevo juego
- H: Historial
- Q: Salir (Q de QUIT)

**replay:**

 Esta función le permite al usuario decidir si quiere jugar una nueva partida después de haber perdido o ganado.
- En caso de no querer jugar más se debe pulsar N, el jugador volverá al menú principal.
- En caso de querer continuar, se debe pulsar Y para iniciar una nueva partida (Esto guarda tu nombre de usuario).
- Si se introduce una opción no válida se le indicará de ésto al usuario y se ejecuta la función nuevamente.

**languagedata:** Pide al usuario que introduzca el idioma en el que desea jugar. Devuelve un valor tipo INT (1 o 2) para que se use
en la función game (el juego).

**useradd:** El usuario introduce su nombre de jugador. Se añade el valor a la variable intname.

**historyadd:** Añade un espacio de historial.

**history:** 
Esta función se divide en dos partes, verifica si el archivo de partidas jugadas (matches.txt) está vacío o no:
-  En caso de estarlo le indica al usuario que no hay partidas guardadas.
-  En caso de no estarlo le muestra al usuario el historial de partidas jugadas.

**achievements:** Ésta función es una pantalla con los distintos logros del juego, aunque de momento, al no estar implementada la mecánica de logros, es un elemento decorativo.

**game:** Esta función se encarga del juego, tiene variables muy importantes:

-  lang: Ésta variable tiene su origen de la selección de idioma. Puede tener dos valores:
    -  1: Español
    -  2: Inglés
    

-  lifes: Inicia como 6, cada vez que el usuario se equivoque perderá una vida (lifes--).

- counter_intword: Inicia como 0, esto es para contar cuántas veces el usuario ha introducido una palabra con más (o menos) de 5 caracteres. Sólo sirve para una pequeña broma en la cual Mr.Wordle se enoja y reduce las vidas del jugador a 0. De haberse implementado el sistema de logros ésto activaría su respectivo logro.

- wincondition: Inicia como null, ésta función permite que el bucle se mantenga en ejecución.
    - Si es true: Se rompe el bucle, se registra en Matchdata y se considera victoria.
    - Si es false: Se rompe el bucle, se registra en Matchdata y se considera derrota.
    - Si es null: Se mantiene el bucle.

- wordsdatabase: Es el archivo donde se almacenan las palabras, dependiendo del idioma seleccionado tendrá el valor del archivo de palabras en inglés o en español.

- Matchdata: Datos de la partida, ordenado en: Nombre, vidas, palabra, idioma, condición de victoria.

