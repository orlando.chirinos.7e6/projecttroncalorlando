
fun decidenewgame(){

    /*
        Deja al usuario varias opciones a elegir, al introducir una de éstas se ejecuta la función correspondiente, caso
        contrario se ejecuta nuevamente ésta misma función. Se pudo haber hecho con un simple WHEN, a corregir.

        L: Logros
        N: Nuevo juego
        H: Historial
        Q: Salir (Q de QUIT)
     */

    println("Seleccione a continuación la sección a la que desea acceder:\n\n" +
            "${Kolor.foreground("Logros",Color.LIGHT_CYAN)}: ¡Pulse [L] para visualizar aquellos logros que ha conseguido a lo largo del juego!(En desarrollo...)\n" +
            "${Kolor.foreground("Nuevo juego",Color.LIGHT_MAGENTA)}: Pulse [N] para iniciar una nueva partida.\n" +
            "${Kolor.foreground("Historial",Color.LIGHT_YELLOW)}: Pulse [H] para mostrar el historial de partidas.\n" +
            "${Kolor.foreground("Salir", Color.RED)}: Pulse [Q] para salir del programa.\n"
            )

    //==============================
    //          SELECCIÓN
    //==============================
    print("¿Cómo desea proceder?: ")
    //===================================

    var choice = scan.next().toLowerCase()

    //===================================
    if (choice == "l"){
        achievements()
    }
    else if (choice == "n"){
        println(Kolor.foreground("====================================================================",Color.WHITE))
        print("¿Está seguro de querer empezar una nueva partida? [Y] / [N]: ")
        //==============================
        val newgame = scan.next().toLowerCase()
        //===============================
        if (newgame == "n"){
            println("\n${Kolor.foreground("Volviendo a pantalla de selección.",Color.LIGHT_YELLOW)}")
            println(Kolor.foreground("====================================================================",Color.WHITE))
            decidenewgame()
        }
        else if(newgame == "y"){
            println(Kolor.foreground("====================================================================",Color.WHITE))
            useradd()
            game(languagedata())
        }

        else{
            println(Kolor.foreground("====================================================================",Color.WHITE))
            println(Kolor.foreground("Instrucción introducida no admisible", Color.LIGHT_YELLOW))
            println(Kolor.foreground("====================================================================",Color.WHITE))
            decidenewgame()
        }
    }

    else if(choice == "q"){
        println(Kolor.foreground("====================================================================",Color.WHITE))
        print("${Kolor.foreground("¿Está seguro de querer abandonar?", Color.LIGHT_YELLOW)} [Y] / [N]: ")
        choice = scan.next().toLowerCase()
        when (choice){
            "y" -> {println()}
            "n" -> {
                println(Kolor.foreground("====================================================================",Color.WHITE))
                decidenewgame()
            }
            else -> {
                println(Kolor.foreground("Instrucción introducida no admisible", Color.YELLOW))
                decidenewgame()}
        }
    }
    else if (choice == "h"){
        history()
    }

    else{
        println(Kolor.foreground("Instrucción introducida no admisible.", Color.LIGHT_RED))
        println(Kolor.foreground("====================================================================",Color.WHITE))
        decidenewgame()
    }
}
fun replay(){

    /*
        Esta función le permite al usuario decidir si quiere jugar una nueva partida después de haber perdido o ganado.
        En caso de no querer jugar más se debe pulsar N, el jugador volverá al menú principal.
        En caso de querer continuar, se debe pulsar Y para iniciar una nueva partida (Esto guarda tu nombre de usuario).
        Si se introduce una opción no válida se le indicará de ésto al usuario y se ejecuta la función nuevamente.
     */

    print("¿Le gustaría iniciar una nueva partida? [Y] / [N]: ")
    val replay = scan.next().toLowerCase()

    if (replay == "n") {
        println("\n${Kolor.foreground("Entendido, volviendo al menú principal ", Color.LIGHT_YELLOW)}...")
        println()
        main()
        }
    else if(replay == "y"){
        println("\n¡Fantástico! ${Kolor.foreground("Empezando nueva partida", Color.LIGHT_YELLOW)}...")
        println(Kolor.foreground("====================================================================",Color.WHITE))
        game(languagedata())
        }
    else{
        println(Kolor.foreground("Instrucción introducida no admisible.", Color.LIGHT_RED))
        println(Kolor.foreground("====================================================================",Color.WHITE))
        replay()
        }
}

