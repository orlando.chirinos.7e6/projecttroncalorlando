fun achievements(){

    /*
        Ésta función es una pantalla con los distintos logros del juego, aunque de momento, al no estar implementada la
        mecánica de logros, es un elemento decorativo.
     */

    //=================WARNING=================
    println(Kolor.foreground("====================================================================",Color.WHITE))
    println("\n${Kolor.background(Kolor.foreground("This section is under construction, no achievements implemented.",Color.BLACK),Color.RED)}\n")
    println(Kolor.foreground("====================================================================",Color.WHITE))
    //=========================================
    println("${Kolor.foreground("Nuevo por aquí",Color.CYAN)}: Inicia sesión y juega tu primera partida de WORDLE.\n" +
            "${Kolor.foreground("¡Qué mala suerte!", Color.CYAN)}: Has perdido tu primera partida de WORDLE.\n" +
            "${Kolor.foreground("¡Victoria!", Color.CYAN)}: Has ganado tu primera partida de WORDLE.\n" +
            "${Kolor.foreground("Sólo es mala suerte", Color.LIGHT_CYAN)}: Has perdido 5 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("Skill issue", Color.LIGHT_RED)}: Has perdido 10 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("¡En racha!", Color.LIGHT_CYAN)}: Has ganado 5 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("GG EZ", Color.LIGHT_YELLOW)}: Has ganado 10 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("Buscando el chiste...", Color.RED)}: Has molestado a Mr.Wordle.")
    println(Kolor.foreground("====================================================================",Color.WHITE))
    println("\n${Kolor.background(Kolor.foreground("Going back to selection screen...",Color.BLACK),Color.RED)}\n")
    println(Kolor.foreground("====================================================================",Color.WHITE))
    decidenewgame()
}