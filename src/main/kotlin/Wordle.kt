/*
    Nombre: Orlando David
    Apellidos: Chirinos Cisneros
    Módulo: M03
    Fecha de inicio: 11/01/2023
    Fecha de finalización: 12/02/2023.
 */

//Función "Random": https://www.develou.com/random-en-kotlin/
//Función ".any"  : https://www.develou.com/funcion-any-en-kotlin/

fun main() {

    //Menú principal, ejecuta la función decidenewgame.

    println("===========================================================================================================================")

    println("Bienvenido a Wordle! A continuación, se explicará en qué consiste el juego: \n \nSe generará una palabra " +
            "aleatoria de 5 caracteres. \nDeberá introducir, por lo tanto, la palabra generada." +
            "\nDispone de ${Kolor.foreground("6", Color.RED)} vidas para cumplir éste objetivo.\n" +
            "El juego finaliza con usted como ${Kolor.foreground("ganador",Color.LIGHT_GREEN)} si cumple dicho objetivo" +
            ", caso contrario ${Kolor.foreground("será fin de partida.",Color.RED)}\n")
    println(
        "El fondo tendrá un color u otro donde:\n ${
            Kolor.background(
                Kolor.foreground("Verde", Color.BLACK),
                Color.GREEN
            )
        }: Representa que la palabra y su posicion son correctos.\n ${
            Kolor.background(
                Kolor.foreground("Gris ", Color.BLACK),
                Color.DARK_GRAY
            )
        }: Representa que la letra existe pero no en la posición correcta.\n ${
            Kolor.background(
                Kolor.foreground("Rojo ", Color.BLACK),
                Color.RED
            
            )
        }: Representa que la letra no existe."
    )
    println("===========================================================================================================================")
    println("")
    decidenewgame()

}

