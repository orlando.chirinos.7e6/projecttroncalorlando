
import java.io.File
import java.util.*

//================================
var Matchdata = ""
val scangame = Scanner(System.`in`)
//================================
fun game(lang: Int){

    /*
        Esta función se encarga del juego, tiene variables muy importantes:

        lang: Ésta variable tiene su origen de la selección de idioma. Puede tener dos valores:
              1: Español
              2: Inglés

        lifes: Inicia como 6, cada vez que el usuario se equivoque perderá una vida (lifes--).

        counter_intword: Inicia como 0, esto es para contar cuántas veces el usuario ha introducido una palabra
                         con más (o menos) de 5 caracteres. Sólo sirve para una pequeña broma en la cual Mr.Wordle se
                         enoja y reduce las vidas del jugador a 0. De haberse implementado el sistema de logros ésto
                         activaría su respectivo logro.

        wincondition: Inicia como null, ésta función permite que el bucle se mantenga en ejecución.
                      Si es true: Se rompe el bucle, se registra en Matchdata y se considera victoria.
                      Si es false: Se rompe el bucle, se registra en Matchdata y se considera derrota.
                      Si es null: Se mantiene el bucle.

        wordsdatabase: Es el archivo donde se almacenan las palabras, dependiendo del idioma seleccionado tendrá el
                       valor del archivo de palabras en inglés o en español.

        Matchdata: Datos de la partida, ordenado en: Nombre, vidas, palabra, idioma, condición de victoria.

     */
    //==============================
    var lifes = 6
    var counter_intword = 0
    var wincondition :Boolean? = null
    var wordsdatabase = File("")

    when (lang){
        1 -> wordsdatabase = File("./src/main/DATAWORDS/palabras.txt")
        2 -> wordsdatabase = File("./src/main/DATAWORDS/englishwords.txt")
                }

    val words: List<String> = wordsdatabase.readLines()

    //================================
    val generatedword = words.random().toString().toLowerCase()
    println(Kolor.foreground("====================================================================",Color.WHITE))
    println(Kolor.background(Kolor.foreground("La palabra ha sido generada.",Color.BLACK),Color.YELLOW))
    println(Kolor.foreground("====================================================================",Color.WHITE))
    //================================

    //==========================================================

    while (wincondition == null){
        when (counter_intword){
            1,2 -> println("\nTamaño de palabra no válido. ¡La palabra introducida ${Kolor.foreground("ha de ser de 5 caracteres",Color.RED)}!\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            3 -> println("\nNuevamente, la palabra introducida ${Kolor.foreground("ha de ser de 5 caracteres",Color.LIGHT_RED)}!\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            4 -> println("\nSupongo que sabrás leer, porque si no, no estarías aquí, la palabra introducida ${Kolor.foreground("ha de ser de 5 caracteres",Color.LIGHT_RED)}!\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            5 -> println("\nYa sabes ${Kolor.foreground("de cuantos caracteres",Color.LIGHT_RED)} debe ser la palabra.\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            6 -> println("\nÚltima advertencia. ${Kolor.foreground("5 CARACTERES",Color.LIGHT_RED)}\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
                7 -> {
                println(Kolor.foreground("\n¿Querías lograr algo, no? Felicidades, lo has logrado.", Color.LIGHT_RED))
                lifes = 0
                wincondition = false
            }
        }
        if (counter_intword < 7){
            //=============================
            println()
            var verifyword = ""
            print("Introduzca palabra: ")
            //=============================
            var intchar = scangame.nextLine().toLowerCase()
            verifyword += intchar

            if (verifyword.lastIndex != 4){
                counter_intword++}

            else if (verifyword == generatedword){

                wincondition = true
                println(Kolor.background(Kolor.foreground(verifyword.toUpperCase(),Color.BLACK),Color.GREEN))}

            else{
                counter_intword = 0
                lifes--

                if (verifyword.lastIndex != 4){
                    counter_intword++}

                else {
                    println()
                    for (i in 0..4){
                        if (verifyword[i] == generatedword[i]){
                            print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.GREEN))
                        }
                        else {
                            when (verifyword[i]){
                                generatedword[0] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[1] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[2] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[3] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[4] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                else -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.RED))
                            }
                        }
                    }
                }
                println()
                when (lifes){
                    5,4 -> println("\n¡Te has equivocado! Vidas restantes: ${Kolor.foreground(lifes.toString(),Color.GREEN)}\n" +
                            Kolor.foreground("====================================================================",Color.WHITE))
                    3,2 -> println("\n¡Cuidado! Vidas restantes: ${Kolor.foreground(lifes.toString(),Color.LIGHT_YELLOW)}\n" +
                            Kolor.foreground("====================================================================",Color.WHITE))
                    1 -> println("\n¡Último intento! ¡Tienes sólo ${Kolor.foreground(lifes.toString(),Color.RED)} vida restante!\n" +
                            Kolor.foreground("====================================================================",Color.WHITE))
                    0 -> wincondition = false
                }

            }
        }
    }

    when (wincondition){
        true -> println("\n¡Felicidades, ${Kolor.foreground("HAS GANADO", Color.LIGHT_GREEN)}!")
        false -> println("\n¡Qué mala suerte, ${Kolor.foreground("HAS PERDIDO", Color.RED)}! La palabra correcta era: ${Kolor.foreground(generatedword.toString().toUpperCase(),Color.MAGENTA)} ")
    }

    if (counter_intword >= 7){println(Kolor.foreground("Suerte a la próxima, crack.",Color.RED))}

    println(Kolor.foreground("\n====================================================================",Color.WHITE))

    Matchdata = "$intname, $lifes, $generatedword, $langchoice, $wincondition\n"
    historyadd()
    replay()


    //==========================================================
}

